# ASP.NET Core Web API with EF Core and SQL Server

## Overview
This project is an ASP.NET Core Web API integrated with Entity Framework Core and SQL Server. It demonstrates CRUD operations on a product catalog.

## Features
- CRUD operations for a product catalog using EF Core.
- SQL Server database integration.
- Configurable logging levels for detailed insights into EF Core operations.

## Prerequisites
- .NET 6.0 SDK or later.
- Visual Studio 2019 or later with ASP.NET and web development workload.
- SQL Server (LocalDB, Express, or other editions).

## Running the Application

1. **Clone the Repository**: Clone this repository to your local machine or download the source code.

2. **Open the Project**: Open the solution file (`YourSolution.sln`) in Visual Studio.

3. **Restore NuGet Packages**: Make sure all NuGet packages are restored.

4. **Database Setup**: Update the connection string in `appsettings.json` to point to your SQL Server instance. Apply EF Core migrations if necessary, or update the database using the `Update-Database` command in the Package Manager Console.

5. **Start the Application**: Build and run the application in Visual Studio. You can use either IIS Express.

## Using the API
The API provides endpoints for managing products:

- `GET /api/products`: Retrieves all products.
- `GET /api/products/{id}`: Retrieves a product by its ID.
- `POST /api/products`: Adds a new product.
- `PUT /api/products/{id}`: Updates an existing product.
- `DELETE /api/products/{id}`: Deletes a product by its ID.

Use tools like Postman or Swagger to interact with these endpoints.

## Configuring Logging Levels

The logging levels in the application can be changed to observe different outputs, particularly for EF Core operations.

1. **Modify `appsettings.json`**
   - Open the `appsettings.json` file in the project.
   - Under the `Logging` section, adjust the log levels. For example, set `Microsoft.EntityFrameworkCore` to `Debug` for verbose EF Core logs.

2. **Restart and Observe**
   - After changing the logging levels, restart the application.
   - Perform API operations and observe the console output in Visual Studio for detailed logs.

## Contributing
Contributions to this project are welcome. Please feel free to fork, make your changes, and submit a pull request. For significant changes, please open an issue first to discuss what you would like to change.

## License
This project is licensed under the [MIT License](LICENSE).
