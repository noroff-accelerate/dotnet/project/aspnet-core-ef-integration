﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Noroff.Samples.ASPNET.EFIntegration.Data
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
        : base(options)
        { }

        public DbSet<Product> Products { get; set; }
    }

}
