﻿using Noroff.Samples.ASPNET.EFIntegration.Data;
namespace Noroff.Samples.ASPNET.EFIntegration.Data
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
